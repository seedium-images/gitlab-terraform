FROM wata727/tflint as tflint-builder

FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest

LABEL maintainer Kostya Zgara <k.zgara@seedium.io>

COPY --from=tflint-builder /usr/local/bin/tflint /usr/local/bin

RUN apk update && \
	apk add --no-cache \
		ca-certificates \
		bash \
		curl \
		git \
    less \
    python3 \
		&& \
	git clone https://github.com/tfutils/tfenv.git ~/.tfenv && \
  rm /usr/local/bin/terraform && \
  ln -s ~/.tfenv/bin/* /usr/local/bin && \
	rm -rf /var/cache/apk/*

ENTRYPOINT []
